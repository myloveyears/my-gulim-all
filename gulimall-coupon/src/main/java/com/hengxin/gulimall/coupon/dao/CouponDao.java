package com.hengxin.gulimall.coupon.dao;

import com.hengxin.gulimall.coupon.entity.CouponEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 优惠券信息
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 18:50:38
 */
@Mapper
public interface CouponDao extends BaseMapper<CouponEntity> {
	
}
