package com.hengxin.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.gulimall.coupon.entity.SeckillSkuRelationEntity;

import java.util.Map;

/**
 * 秒杀活动商品关联
 *
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 18:50:38
 */
public interface SeckillSkuRelationService extends IService<SeckillSkuRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

