package com.hengxin.gulimall.coupon;

import com.hengxin.gulimall.coupon.entity.CouponEntity;
import com.hengxin.gulimall.coupon.service.CouponService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class GulimallCouponApplicationTests {

    @Autowired
    CouponService couponService;

    @Test
    void contextLoads() {
        CouponEntity couponEntity = new CouponEntity();
        couponService.getById(couponEntity);
        System.out.println(200);
    }

}
