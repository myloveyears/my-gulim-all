package com.hengxin.gulimall.gateway;

import com.alibaba.nacos.api.config.annotation.NacosValue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
class GulimallGatewayApplicationTests {

    @Value("${server.port}")
    private String num;

    @Test
    void contextLoads() {
        System.out.println(num);
    }

}
