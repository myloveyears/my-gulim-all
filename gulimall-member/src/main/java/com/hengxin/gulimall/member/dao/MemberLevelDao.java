package com.hengxin.gulimall.member.dao;

import com.hengxin.gulimall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 19:10:16
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
