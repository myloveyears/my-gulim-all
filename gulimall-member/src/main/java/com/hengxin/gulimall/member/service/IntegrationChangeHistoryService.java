package com.hengxin.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.gulimall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 19:10:15
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

