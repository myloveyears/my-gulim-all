package com.hengxin.gulimall.order.dao;

import com.hengxin.gulimall.order.entity.OmsOrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:47:14
 */
@Mapper
public interface OmsOrderSettingDao extends BaseMapper<OmsOrderSettingEntity> {
	
}
