package com.hengxin.gulimall.order.dao;

import com.hengxin.gulimall.order.entity.OmsRefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:47:13
 */
@Mapper
public interface OmsRefundInfoDao extends BaseMapper<OmsRefundInfoEntity> {
	
}
