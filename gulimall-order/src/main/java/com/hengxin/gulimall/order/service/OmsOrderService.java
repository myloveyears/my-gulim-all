package com.hengxin.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.gulimall.order.entity.OmsOrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:47:14
 */
public interface OmsOrderService extends IService<OmsOrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

