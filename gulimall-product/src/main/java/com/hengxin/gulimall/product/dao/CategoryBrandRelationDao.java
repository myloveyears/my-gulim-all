package com.hengxin.gulimall.product.dao;

import com.hengxin.gulimall.product.entity.CategoryBrandRelationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 品牌分类关联
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 17:39:14
 */
@Mapper
public interface CategoryBrandRelationDao extends BaseMapper<CategoryBrandRelationEntity> {

//    void updateCategory(Long catId, String name);

    void updateCategory(@Param("catId") Long catId, @Param("name") String name);
}
