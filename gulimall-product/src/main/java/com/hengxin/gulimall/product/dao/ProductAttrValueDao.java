package com.hengxin.gulimall.product.dao;

import com.hengxin.gulimall.product.entity.ProductAttrValueEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu属性值
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 17:39:15
 */
@Mapper
public interface ProductAttrValueDao extends BaseMapper<ProductAttrValueEntity> {
	
}
