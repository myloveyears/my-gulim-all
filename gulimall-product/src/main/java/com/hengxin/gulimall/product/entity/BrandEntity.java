package com.hengxin.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.hengxin.common.valid.AddGro;
import com.hengxin.common.valid.ListValue;
import com.hengxin.common.valid.UpdateGroup;
import com.hengxin.common.valid.UpdateStatusGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;


import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 17:39:14
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@NotNull(message = "修改必须指定品牌id",groups = {UpdateGroup.class})
	@Null(message = "新增不能指定id",groups = {AddGro.class})
	@TableId
	private Long brandId;
	/**
	 * 品牌名
	 */
	// 不能为空 或 空串
//	@NotEmpty
	// 一个非空字符
	@NotBlank(message = "品牌名必须提交",groups = {AddGro.class,UpdateGroup.class})
	private String name;
	/**
	 * 品牌logo地址
	 */
	@NotBlank(groups = {AddGro.class})
	@URL(message = "logo必须是一个合法的URL地址",groups = {AddGro.class,UpdateGroup.class})
	private String logo;
	/**
	 * 介绍
	 */
	private String descript;
	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(groups = {AddGro.class, UpdateStatusGroup.class})
	@ListValue(vals={0,1},groups = {AddGro.class, UpdateStatusGroup.class})
	private Integer showStatus;
	/**
	 * 检索首字母
	 */
	@NotNull(groups = {AddGro.class})
	@Pattern(regexp = "^[a-zA-Z]$",message = "检索首字母必须是一个字母",groups = {AddGro.class,UpdateGroup.class})
	private String firstLetter;
	/**
	 * 排序
	 */
	@NotNull(groups = {AddGro.class})
	@Min(value = 0,message = "排序必须大于等于0",groups = {AddGro.class,UpdateGroup.class})
	private Integer sort;

}
