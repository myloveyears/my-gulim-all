package com.hengxin.gulimall.product.exception;

import com.hengxin.common.exception.BizCodeEnume;
import com.hengxin.common.utils.R;
import com.hengxin.gulimall.product.entity.BrandEntity;
import com.hengxin.gulimall.product.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 统一处理异常
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.hengxin.gulimall.product.exception")
public class GulimallExceptionControllerAdvice {

//    @ExceptionHandler(value = MethodArgumentNotValidException.class)
//    public R handleVaildException(MethodArgumentNotValidException e){
//        log.error("数据校验出现问题{}",e.getMessage(),e.getClass());
//        BindingResult bindingResult = e.getBindingResult();
//
//        Map<String,String> errorMap = new HashMap<>();
//        bindingResult.getFieldErrors().forEach((item)->{
//            errorMap.put(item.getField(), item.getDefaultMessage());
//        });
//        return R.error(BizCodeEnume.VAILD_EXCEPTION.getCode(), BizCodeEnume.VAILD_EXCEPTION.getMsg()).put("data",errorMap);
//    }
//
//    @ExceptionHandler(value = Throwable.class)
//    public R handleException(Throwable throwable){
//        log.error("错误：",throwable);
//        return R.error(BizCodeEnume.UNKNOW_EXCEPTION.getCode(), BizCodeEnume.UNKNOW_EXCEPTION.getMsg());
//    }
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public R handleValidException(MethodArgumentNotValidException e) {
        log.error("数据校验出现问题{},异常类型{}", e.getMessage(), e.getClass());
        BindingResult bindingResult = e.getBindingResult();
        Map<String, String> errorMap = new HashMap<>();
        bindingResult.getFieldErrors().forEach(fieldError -> {
            errorMap.put(fieldError.getField(), fieldError.getDefaultMessage());
        });
        return R.error(BizCodeEnume.VAILD_EXCEPTION.getCode(), BizCodeEnume.VAILD_EXCEPTION.getMsg()).put("data", errorMap);
    }

    @ExceptionHandler(value = Throwable.class)
    public R handleException(Throwable throwable) {
        return R.error(BizCodeEnume.UNKNOW_EXCEPTION.getCode(), BizCodeEnume.UNKNOW_EXCEPTION.getMsg());
    }
}
