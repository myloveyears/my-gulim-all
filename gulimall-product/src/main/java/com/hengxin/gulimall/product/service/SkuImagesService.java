package com.hengxin.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.gulimall.product.entity.SkuImagesEntity;

import java.util.Map;

/**
 * sku图片
 *
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 17:39:15
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

