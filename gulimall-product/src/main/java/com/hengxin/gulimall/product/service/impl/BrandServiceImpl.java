package com.hengxin.gulimall.product.service.impl;


import com.hengxin.gulimall.product.service.CategoryBrandRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.common.utils.Query;

import com.hengxin.gulimall.product.dao.BrandDao;
import com.hengxin.gulimall.product.entity.BrandEntity;
import com.hengxin.gulimall.product.service.BrandService;
import org.springframework.util.StringUtils;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //1.获取key
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> brandEntityQuery = new QueryWrapper<>();
        if(!StringUtils.isEmpty(key)){
            brandEntityQuery.eq("brand_id",key).or().like("name",key);
        }

        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                brandEntityQuery
        );

        return new PageUtils(page);
    }

    @Override
    public void updateByDetail(BrandEntity brand) {
        // 保证冗余字段数据一致性
        this.updateById(brand);
        if(!StringUtils.isEmpty(brand.getName())){
            //同步更新其他关联表中的数据
            categoryBrandRelationService.updateBrand(brand.getBrandId(),brand.getName());

            //TODO 更新其他关联
        }
    }

}