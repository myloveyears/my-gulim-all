package com.hengxin.gulimall.thirdparty;

import com.aliyun.oss.OSSClient;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.InputStream;

@SpringBootTest
class GulimallThirdPartyApplicationTests {

    @Autowired
    OSSClient ossClient;

    @Test
    void contextLoads() {
        /**
         * 1.引入 oss-starter
         * 2.配置key，endpoint相关信息即可
         * 3.使用OSSClient
         */
        try {
            /**
             * 1.引入 oss-starter
             * 2.配置key，endpoint相关信息即可
             * 3.使用OSSClient
             */
            // 创建OSSClient实例。
            //OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

            // 填写本地文件的完整路径。如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件流。
            InputStream inputStream = new FileInputStream("C:\\Users\\Administrator\\Desktop\\db4f08640f0a338912a4d4250a48b1c.jpg");
            // 依次填写Bucket名称（例如examplebucket）和Object完整路径（例如exampledir/exampleobject.txt）。Object完整路径中不能包含Bucket名称。
            ossClient.putObject("gulimall-heng", "db4f08640f0a338912a4d4250a48b1c.jpg", inputStream);

            // 关闭OSSClient。
            ossClient.shutdown();

            System.out.println("上传完成...");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
