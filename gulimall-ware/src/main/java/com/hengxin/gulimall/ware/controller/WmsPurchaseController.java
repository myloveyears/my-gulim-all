package com.hengxin.gulimall.ware.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.hengxin.gulimall.ware.vo.MergeVo;
import com.hengxin.gulimall.ware.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.hengxin.gulimall.ware.entity.WmsPurchaseEntity;
import com.hengxin.gulimall.ware.service.WmsPurchaseService;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.common.utils.R;



/**
 * 采购信息
 *
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:55:10
 */
@RestController
@RequestMapping("ware/purchase")
public class WmsPurchaseController {
    @Autowired
    private WmsPurchaseService wmsPurchaseService;

    /**
     * 合并采购需求订单列表
     * @param ids
     * @return
     */
    @PostMapping("/received")
    public R received(@RequestBody List<Long> ids){

        wmsPurchaseService.received(ids);

        return R.ok();
    }

    /**
     * 完成采购
     * @param doneVo
     * @return
     */
    @PostMapping("/done")
    public R done(@RequestBody PurchaseDoneVo doneVo){

        wmsPurchaseService.done(doneVo);

        return R.ok();
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    //@RequiresPermissions("ware:wmspurchase:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = wmsPurchaseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 合并采购需求订单列表
     * @param params
     * @return
     */
    @RequestMapping("/unreceive/list")
    //@RequiresPermissions("ware:wmspurchase:list")
    public R unreceivelist(@RequestParam Map<String, Object> params){
        PageUtils page = wmsPurchaseService.queryPageUnreceive(params);

        return R.ok().put("page", page);
    }
    /**
     * 合并采购需求订单列确认
     */
    @PostMapping(("/merge"))
    //@RequiresPermissions("ware:wmspurchase:list")
    public R merge(@RequestBody MergeVo mergeVo){
        wmsPurchaseService.mergePurchase(mergeVo);

        return R.ok();
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    //@RequiresPermissions("ware:wmspurchase:info")
    public R info(@PathVariable("id") Long id){
		WmsPurchaseEntity wmsPurchase = wmsPurchaseService.getById(id);

        return R.ok().put("wmsPurchase", wmsPurchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    //@RequiresPermissions("ware:wmspurchase:save")
    public R save(@RequestBody WmsPurchaseEntity wmsPurchase){
        wmsPurchase.setUpdateTime(new Date());
        wmsPurchase.setCreateTime(new Date());
		wmsPurchaseService.save(wmsPurchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    //@RequiresPermissions("ware:wmspurchase:update")
    public R update(@RequestBody WmsPurchaseEntity wmsPurchase){
		wmsPurchaseService.updateById(wmsPurchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    //@RequiresPermissions("ware:wmspurchase:delete")
    public R delete(@RequestBody Long[] ids){
		wmsPurchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
