package com.hengxin.gulimall.ware.dao;

import com.hengxin.gulimall.ware.entity.WmsPurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:55:10
 */
@Mapper
public interface WmsPurchaseDao extends BaseMapper<WmsPurchaseEntity> {
	
}
