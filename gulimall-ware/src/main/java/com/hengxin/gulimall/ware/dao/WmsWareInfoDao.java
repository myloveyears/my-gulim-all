package com.hengxin.gulimall.ware.dao;

import com.hengxin.gulimall.ware.entity.WmsWareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:55:09
 */
@Mapper
public interface WmsWareInfoDao extends BaseMapper<WmsWareInfoEntity> {
	
}
