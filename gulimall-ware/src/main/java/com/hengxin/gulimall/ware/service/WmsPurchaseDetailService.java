package com.hengxin.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.gulimall.ware.entity.WmsPurchaseDetailEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:55:09
 */
public interface WmsPurchaseDetailService extends IService<WmsPurchaseDetailEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<WmsPurchaseDetailEntity> listDetailByPurchaseId(Long id);
}

