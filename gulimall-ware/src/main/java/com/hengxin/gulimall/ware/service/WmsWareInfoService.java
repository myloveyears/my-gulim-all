package com.hengxin.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.gulimall.ware.entity.WmsWareInfoEntity;

import java.util.Map;

/**
 * 仓库信息
 *
 * @author hengxin
 * @email 1056065827@qq.com
 * @date 2022-01-10 22:55:09
 */
public interface WmsWareInfoService extends IService<WmsWareInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

