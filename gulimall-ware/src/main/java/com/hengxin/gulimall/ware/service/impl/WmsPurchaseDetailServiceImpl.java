package com.hengxin.gulimall.ware.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hengxin.common.utils.PageUtils;
import com.hengxin.common.utils.Query;

import com.hengxin.gulimall.ware.dao.WmsPurchaseDetailDao;
import com.hengxin.gulimall.ware.entity.WmsPurchaseDetailEntity;
import com.hengxin.gulimall.ware.service.WmsPurchaseDetailService;
import org.springframework.util.StringUtils;


@Service("wmsPurchaseDetailService")
public class WmsPurchaseDetailServiceImpl extends ServiceImpl<WmsPurchaseDetailDao, WmsPurchaseDetailEntity> implements WmsPurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WmsPurchaseDetailEntity> queryWrapper = new QueryWrapper<WmsPurchaseDetailEntity>();
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
            queryWrapper.and(w->{
                w.eq("purchase_id",key).or().eq("sku_id",key);
            });
        }

        String status = (String) params.get("status");
        if(!StringUtils.isEmpty(status)){
            queryWrapper.eq("status",status);
        }
        String wareId = (String) params.get("wareId");
        if(!StringUtils.isEmpty(status)){
            queryWrapper.eq("ware_id",wareId);
        }
        IPage<WmsPurchaseDetailEntity> page = this.page(
                new Query<WmsPurchaseDetailEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public List<WmsPurchaseDetailEntity> listDetailByPurchaseId(Long id) {
        List<WmsPurchaseDetailEntity> purchaseId = this.list(new QueryWrapper<WmsPurchaseDetailEntity>().eq("purchase_id", id));
        return purchaseId;
    }

}